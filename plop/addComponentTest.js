module.exports = {
  description: 'Add\'s a component unit test',
  prompts: [
    {
      type: 'input',
      name: 'name',
      message: 'What is the component\'s name?',
    },
    {
      type: 'input',
      name: 'module',
      message: 'What is the module name? (Leave blank for common component)',
    },
  ],
  actions: [
    {
      type: 'add',
      path: '{{#if module}}src/{{module}}/components/__tests__/{{pascalCase name}}.spec.jsx{{else}}src/components/__tests__/{{pascalCase name}}.spec.jsx{{/if}}',
      templateFile: 'plop/templates/Component-test.js.hbs',
    },
  ],
}
