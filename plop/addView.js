module.exports = {
  description: 'Creates a view React Component',
  prompts: [
    {
      type: 'input',
      name: 'name',
      message: 'What is the view\'s name?',
    },
    {
      type: 'input',
      name: 'module',
      message: 'What is the module name? (Leave blank for common component)',
    },
  ],
  actions: [
    {
      type: 'add',
      path: 'src/{{module}}/{{pascalCase name}}/{{pascalCase name}}.jsx',
      templateFile: 'plop/templates/ViewComponent.js.hbs',
    },
    {
      type: 'add',
      path: 'src/{{module}}/{{pascalCase name}}/index.js',
      templateFile: 'plop/templates/ViewComponent-index.js.hbs',
    },
  ],
}
