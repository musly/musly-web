module.exports = {
  description: 'Creates a React Component',
  prompts: [
    {
      type: 'input',
      name: 'name',
      message: 'What is the component\'s name?',
    },
    {
      type: 'input',
      name: 'module',
      message: 'What is the module name? (Leave blank for common component)',
    },
  ],
  actions: [
    {
      type: 'add',
      path: '{{#if module}}src/{{module}}/components/{{pascalCase name}}/{{pascalCase name}}.jsx{{else}}src/components/{{pascalCase name}}/{{pascalCase name}}.jsx{{/if}}',
      templateFile: 'plop/templates/Component.js.hbs',
    },
    {
      type: 'add',
      path: '{{#if module}}src/{{module}}/components/__tests__/{{pascalCase name}}.spec.jsx{{else}}src/components/__tests__/{{pascalCase name}}.spec.jsx{{/if}}',
      templateFile: 'plop/templates/Component-test.js.hbs',
    },
    {
      type: 'add',
      path: '{{#if module}}src/{{module}}/components/{{pascalCase name}}/index.js{{else}}src/components/{{pascalCase name}}/index.js{{/if}}',
      templateFile: 'plop/templates/Component-index.js.hbs',
    },
    {
      type: 'modify',
      path: '{{#if module}}src/{{module}}/components/index.js{{else}}src/components/index.js{{/if}}',
      pattern: /(\/\/ eof)/gi,
      template: 'export { {{pascalCase name}} } from \'./{{pascalCase name}}\'\n$1',
    },
  ],
}
