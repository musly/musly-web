# Musly

[![Coverage Status](https://coveralls.io/repos/gitlab/musly/musly-web/badge.svg?branch=develop)](https://coveralls.io/gitlab/musly/musly-web?branch=develop)

The musly app helps musicians and managers of musical groups to create and organize the their music.

## Development

### Setting up local development environment

Start by installing the project's dependencies. A pre-defined command will take care of everything:

```sh
> make clean
```

Create a `.env` file in the root of the project. This is git-ignored and is only meant for your local project set-up. **DO NOT COMMIT THIS FILE!**

It is needed to establish the connection to the Firebase project.

Add the following contents to the file:

```sh
LOCALE=en // right now, we only support english (en)!
FIREBASE_API_KEY=
FIREBASE_AUTH_DOMAIN=
FIREBASE_DATABASE_URL=
FIREBASE_PROJECT_ID=
FIREBASE_STORGAE_BUCKET=
FIREBASE_MESSAGING_SENDER_ID=
FIREBASE_APP_ID=
FIREBASE_MEASUREMENT_ID=
SENTRY_DSN=
```

The actual values for the Firebase related variables can be found inside the `Firebase Console > Project Settings`
[https://console.firebase.google.com/u/3/project/musly-app-2020/settings/general/web:ZWIxYjRlMzQtNTliNy00MDhmLWI5MmUtZDViYmRiMDQ0ZjBi](https://console.firebase.google.com/u/3/project/musly-app-2020/settings/general/web:ZWIxYjRlMzQtNTliNy00MDhmLWI5MmUtZDViYmRiMDQ0ZjBi)

The sentry.io DSN can be found here: [https://sentry.io/settings/musly/projects/musly/keys/](https://sentry.io/settings/musly/projects/musly/keys/)

### Running the development server

```sh
> yarn start
```

### Running the staging server locally

```sh
> yarn stage
```

### Generators

In order to make it easier to create new components while developing, you can use `generators` to do so. Generators are simple CLI commands that will generate the appropriate component for you.

The command syntax is `yarn generate [subject] [elementName]`. There is also a shorthand
for this: `yarn g [subject] [elementName]`.

#### Example: creating a new React Component**

```sh
> yarn g component loading
```

This will create a new folder `src/components/Loading` and the `Loading.jsx` and `index.js` inside it, with some sample content. Also, it will create a basic rendering unit test for that component inside the `components` folder.

#### Example: creating a new hook**

```sh
> yarn g hook useLoading
```

This will create a new file `src/hooks/useLoading.js` with the appropriate content, and it will
add an export index inside `/src/hooks/index.js`.

#### List of Generators

| Element | Command |
| --- | --- |
| Icon | `yarn g icon [iconName]` |
| Component | `yarn g component [componentName]` |
| View | `yarn g view [viewName]` |
| Hook | `yarn g hook [hookName]` |
