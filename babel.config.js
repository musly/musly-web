module.exports = (api) => {
  api.cache(true)

  return {
    compact: true,
    presets: [
      ['@babel/preset-env', {
        modules: false,
        loose: true,
      }],
      '@babel/preset-react',
    ],
    plugins: [
      'react-hot-loader/babel',
      '@babel/plugin-proposal-class-properties',
      '@babel/plugin-proposal-object-rest-spread',
      '@babel/plugin-transform-runtime',
    ],
    env: {
      test: {
        presets: [
          '@babel/preset-env',
          '@babel/preset-react',
        ],
      },
      production: {
        plugins: [
          'babel-plugin-transform-react-remove-prop-types',
        ],
      },
    },
  }
}
