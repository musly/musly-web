import React from 'react'
import { Image, Box } from '@chakra-ui/core'
import { Header, Navigation } from './components'
import { useUser } from './user/hooks'
import { useGroups } from './group/hooks'
import { UserMenu } from './user/components'

export function AppHeader() {
  const { user } = useUser()
  const { groups } = useGroups()

  if (!user.isLoggedIn || groups.length === 0) {
    return null
  }

  return (
    <Header>
      <Box d="flex" alignItems="center" justifyContent="flex-start" width="220px" pl={4}>
        <Image src="/assets/logo.svg" height="25px" width="auto" />
      </Box>
      <Box d="flex" alignItems="stretch" justifyContent="flex-start" height="full">
        <Navigation />
      </Box>
      <Box d="flex" alignItems="center" justifyContent="flex-end" flexGrow={1} pr={4}>
        <UserMenu />
      </Box>
    </Header>
  )
}
