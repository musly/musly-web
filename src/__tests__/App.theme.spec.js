import theme from '../App.theme'

describe('App.theme', () => {
  it('should be of a certain shape', () => {
    expect(theme).toMatchSnapshot()
  })

  it('should contain "Robot" for body and heading', () => {
    expect(theme.fonts.body).toEqual('Roboto, sans-serif')
    expect(theme.fonts.heading).toEqual('Roboto, sans-serif')
  })

  it('should contain #FFC136 as yellow.500', () => {
    expect(theme.colors.yellow[500]).toEqual('#FFC136')
  })
})
