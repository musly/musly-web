import React from 'react'
import { shallow } from 'enzyme'
import { useUser } from '../user/hooks'
import { useGroups } from '../group/hooks'
import { AppHeader } from '../AppHeader'

jest.mock('../user/hooks', () => ({
  ...jest.requireActual('../user/hooks'),
  useUser: jest.fn(),
}))

jest.mock('../group/hooks', () => ({
  ...jest.requireActual('../group/hooks'),
  useGroups: jest.fn(),
}))

describe('<AppHeader />', () => {
  it('should not render if user is logged out', () => {
    useUser.mockReturnValueOnce({ user: { isLoggedIn: false } })
    useGroups.mockReturnValueOnce({ groups: [] })

    const wrapper = shallow(
      <AppHeader />
    )

    expect(wrapper.instance()).toEqual(null)
    expect(wrapper.find('Header').length).toEqual(0)
    expect(wrapper.instance()).toMatchSnapshot()
  })

  it('should not render if user is logged in but has no groups', () => {
    useUser.mockReturnValueOnce({ user: { isLoggedIn: true } })
    useGroups.mockReturnValueOnce({ groups: [] })

    const wrapper = shallow(
      <AppHeader />
    )

    expect(wrapper.instance()).toEqual(null)
    expect(wrapper.find('Header').length).toEqual(0)
    expect(wrapper.instance()).toMatchSnapshot()
  })

  it('should render if the user is logged in and has groups', () => {
    useUser.mockReturnValueOnce({ user: { isLoggedIn: true } })
    useGroups.mockReturnValueOnce({ groups: ['foo'] })

    const wrapper = shallow(
      <AppHeader />
    )

    expect(wrapper.find('Header').length).toEqual(1)
    expect(wrapper).toMatchSnapshot()
  })
})
