import React from 'react'
import { UserContext } from '../components/UserProvider/UserContext'

export function useUser() {
  const user = React.useContext(UserContext)
  return user
}
