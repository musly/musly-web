import React from 'react'

const Profile = React.lazy(() => import(/* webpackChunkName: "profile" */'./Profile'))

export { Profile }
