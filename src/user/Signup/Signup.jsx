import React, { useCallback, memo } from 'react'
import { Box, Image, Text, Flex, Button } from '@chakra-ui/core'
import { t } from '../../utils/i18n'
import { View, Copyright } from '../../components'
import { useFormState, useLoadingState } from '../../hooks'
import { useUser } from '../hooks'
import { InputEmail, InputPassword, InputPasswordConfirm, InputBirthday, InputDisplayName, SelectGender } from '../components'

const initialState = {
  email: '',
  password: '',
  passwordConfirm: '',
  displayName: '',
  birthday: '',
  gender: '',
}

const Signup = memo(function Signup() {
  const { signup } = useUser()
  const { isLoading } = useLoadingState()

  const onComplete = useCallback((values) => {
    // TODO: Build in error handling
    signup(values)
  }, [signup])

  const { values, onChange, onSubmit } = useFormState(initialState, onComplete)

  return (
    <View title={t('user.signup_title')}>
      <Box w="full" maxW="sm" mx="auto" pt={20}>
        <Image src="/assets/logo-black.svg" mx="auto" mb={8} w={48} h="auto" />
        <Text textAlign="center" fontSize="lg" mb={5}>
          {t('user.signup_headline')}
        </Text>
        <Box
          as="form"
          bg="white"
          shadow="lg"
          rounded="lg"
          p={10}
          mb={6}
          onSubmit={onSubmit}
          autoComplete="off"
        >
          <Box mb={4}>
            <InputEmail value={values.email} onChange={onChange} />
          </Box>
          <Box mb={4}>
            <InputPassword value={values.password} onChange={onChange} />
          </Box>
          <Box mb={6}>
            <InputPasswordConfirm value={values.passwordConfirm} onChange={onChange} />
          </Box>
          <Box mb={4}>
            <InputDisplayName value={values.displayName} onChange={onChange} />
          </Box>
          <Box mb={4}>
            <InputBirthday value={values.birthday} onChange={onChange} />
          </Box>
          <Box mb={6}>
            <SelectGender value={values.gender} onChange={onChange} />
          </Box>
          <Flex align="center" justify="center">
            <Button type="submit" mr={4} flexGrow={1} variantColor="yellow" isLoading={isLoading}>
              {t('user.signup_btn')}
            </Button>
            <Button type="reset" isLoading={isLoading} flexGrow={1}>
              {t('general.reset')}
            </Button>
          </Flex>
        </Box>
        <Box textAlign="center">
          <Copyright />
        </Box>
      </Box>
    </View>
  )
})

export default Signup
