import React, { useState, useCallback } from 'react'
import PropTypes from 'prop-types'
import { FormControl, FormLabel, Input, InputGroup, InputRightElement, IconButton } from '@chakra-ui/core'
import { FaEye, FaEyeSlash } from 'react-icons/fa'
import { t } from '../../../utils/i18n'

export function InputPasswordConfirm({ onChange, value }) {
  const [show, setShow] = useState(false)

  const onClick = useCallback(() => {
    setShow(!show)
  }, [show])

  return (
    <FormControl>
      <FormLabel htmlFor="passwordConfirm" id="password-confirm-label">
        {t('user.password_confirm_label')}
      </FormLabel>
      <InputGroup size="md">
        <Input
          type={show ? 'text' : 'password'}
          id="passwordConfirm"
          name="passwordConfirm"
          aria-describedby="password-confirm-label"
          placeholder={t('user.password_confirm_placeholder')}
          onChange={onChange}
          value={value}
        />
        <InputRightElement width="2.75rem">
          <IconButton
            size="sm"
            h="1.75rem"
            icon={show ? FaEyeSlash : FaEye}
            onClick={onClick}
            aria-label={show ? t('general.hide') : t('general.show')}
          />
        </InputRightElement>
      </InputGroup>
    </FormControl>
  )
}

InputPasswordConfirm.propTypes = {
  onChange: PropTypes.func.isRequired,
  value: PropTypes.string.isRequired,
}
