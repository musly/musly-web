import React from 'react'
import PropTypes from 'prop-types'
import { FormControl, FormLabel, Input } from '@chakra-ui/core'
import { t } from '../../../utils/i18n'

export function InputDisplayName({ onChange, value }) {
  return (
    <FormControl>
      <FormLabel htmlFor="displayName" id="displayName-label">
        {t('user.displayName_label')}
      </FormLabel>
      <Input
        type="text"
        id="displayName"
        name="displayName"
        placeholder={t('user.displayName_placeholder')}
        aria-describedby="displayName-label"
        onChange={onChange}
        value={value}
      />
    </FormControl>
  )
}

InputDisplayName.propTypes = {
  onChange: PropTypes.func.isRequired,
  value: PropTypes.string.isRequired,
}
