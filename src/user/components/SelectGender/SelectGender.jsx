import React from 'react'
import PropTypes from 'prop-types'
import { FormControl, FormLabel, Select } from '@chakra-ui/core'
import { t } from '../../../utils/i18n'

export function SelectGender({ onChange, value }) {
  return (
    <FormControl>
      <FormLabel htmlFor="gender" id="gender-label">
        {t('user.gender_label')}
      </FormLabel>
      <Select
        id="gender"
        name="gender"
        aria-describedby="gender-label"
        onChange={onChange}
        value={value}
      >
        <option value="">
          {t('user.gender_none')}
        </option>
        <option value="male">
          {t('user.gender_male')}
        </option>
        <option value="female">
          {t('user.gender_female')}
        </option>
      </Select>
    </FormControl>
  )
}

SelectGender.propTypes = {
  onChange: PropTypes.func.isRequired,
  value: PropTypes.string.isRequired,
}
