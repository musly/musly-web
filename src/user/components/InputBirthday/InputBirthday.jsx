import React from 'react'
import PropTypes from 'prop-types'
import { FormControl, FormLabel, Input } from '@chakra-ui/core'
import { t } from '../../../utils/i18n'

export function InputBirthday({ onChange, value }) {
  return (
    <FormControl>
      <FormLabel htmlFor="birthday" id="birthday-label">
        {t('user.birthday_label')}
      </FormLabel>
      <Input
        autoComplete="off"
        type="date"
        id="birthday"
        name="birthday"
        placeholder={t('user.birthday_placeholder')}
        aria-describedby="birthday-label"
        onChange={onChange}
        value={value}
      />
    </FormControl>
  )
}

InputBirthday.propTypes = {
  onChange: PropTypes.func.isRequired,
  value: PropTypes.string.isRequired,
}
