import React from 'react'
import { Avatar, Box, Menu, MenuButton, MenuList, MenuGroup, MenuDivider, MenuItem } from '@chakra-ui/core'
import { t } from '../../../utils/i18n'
import { useGravatar, usePush } from '../../../hooks'
import { useUser } from '../../hooks'

export function UserMenu() {
  const { user: { email } = {} } = useUser()
  const src = useGravatar(email)
  const linkToProfile = usePush('/profile')
  const linkToSettings = usePush('/settings')

  return (
    <Box color="gray.900">
      <Menu>
        <MenuButton _focus={{ outline: 0 }}>
          <Avatar src={src} size="sm" />
        </MenuButton>
        <MenuList>
          <MenuGroup>
            <MenuItem onClick={linkToProfile}>
              {t('user.menu_profile')}
            </MenuItem>
          </MenuGroup>
          <MenuDivider />
          <MenuGroup>
            <MenuItem onClick={linkToSettings}>
              {t('user.menu_settings')}
            </MenuItem>
            <MenuItem>
              {t('user.menu_logout')}
            </MenuItem>
          </MenuGroup>
        </MenuList>
      </Menu>
    </Box>
  )
}
