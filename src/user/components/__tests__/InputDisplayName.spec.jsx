import React from 'react'
import { create } from 'react-test-renderer'
import { InputDisplayName } from '../InputDisplayName'

describe('<InputDisplayName />', () => {
  it('should render', () => {
    const wrapper = create(
      <InputDisplayName value="" onChange={jest.fn()} />
    )

    expect(wrapper.toJSON()).toMatchSnapshot()
  })
})
