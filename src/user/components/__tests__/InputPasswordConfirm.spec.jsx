import React from 'react'
import { shallow } from 'enzyme'
import { act } from 'react-test-renderer'
import { InputPasswordConfirm } from '../InputPasswordConfirm'

describe('<InputPasswordConfirm />', () => {
  it('should render', () => {
    const onChange = jest.fn()
    let wrapper

    act(() => {
      wrapper = shallow(
        <InputPasswordConfirm value="SomePassword" onChange={onChange} />
      )
    })

    expect(wrapper).toMatchSnapshot()
  })

  it('should change the input type on button click', () => {
    const onChange = jest.fn()
    let wrapper
    act(() => {
      wrapper = shallow(
        <InputPasswordConfirm value="SomePassword" onChange={onChange} />
      )
    })

    expect(wrapper).toMatchSnapshot()
    // should be type="password" initially
    expect(wrapper.find('Input').props().type).toEqual('password')
    wrapper.find('IconButton').simulate('click')
    // should be type="text" after icon click
    expect(wrapper.find('Input').props().type).toEqual('text')
    wrapper.find('IconButton').simulate('click')
    // should be back to type="password" after second icon click
    expect(wrapper.find('Input').props().type).toEqual('password')
  })
})
