import React from 'react'
import { shallow } from 'enzyme'
import { InputEmail } from '../InputEmail'

describe('<InputEmail />', () => {
  it('should render', () => {
    const onChange = jest.fn()
    const wrapper = shallow(
      <InputEmail value="" onChange={onChange} />
    )

    expect(wrapper).toMatchSnapshot()
  })
})
