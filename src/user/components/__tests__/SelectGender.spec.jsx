import React from 'react'
import { shallow } from 'enzyme'
import { act } from 'react-test-renderer'
import { SelectGender } from '../SelectGender'

describe('<SelectGender />', () => {
  it('should render', () => {
    const onChange = jest.fn()
    let wrapper

    act(() => {
      wrapper = shallow(
        <SelectGender value="" onChange={onChange} />
      )
    })

    expect(wrapper).toMatchSnapshot()
  })
})
