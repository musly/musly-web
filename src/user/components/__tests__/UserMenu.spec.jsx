import React from 'react'
import { shallow } from 'enzyme'
import { act } from 'react-test-renderer'
import { UserMenu } from '../UserMenu'

describe('<UserMenu />', () => {
  it('should render', () => {
    let wrapper

    act(() => {
      wrapper = shallow(
        <UserMenu />
      )
    })

    expect(wrapper).toMatchSnapshot()
  })
})
