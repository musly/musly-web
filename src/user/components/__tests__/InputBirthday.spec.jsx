import React from 'react'
import { shallow } from 'enzyme'
import { InputBirthday } from '../InputBirthday'

describe('<InputBirthday />', () => {
  it('should render', () => {
    const onChange = jest.fn()
    const wrapper = shallow(
      <InputBirthday value="" onChange={onChange} />
    )

    expect(wrapper).toMatchSnapshot()
  })
})
