import React, { useState, useCallback } from 'react'
import PropTypes from 'prop-types'
import { FormControl, FormLabel, Input, InputGroup, InputRightElement, IconButton } from '@chakra-ui/core'
import { FaEye, FaEyeSlash } from 'react-icons/fa'
import { t } from '../../../utils/i18n'

export function InputPassword({ onChange, value }) {
  const [show, setShow] = useState(false)

  const onClick = useCallback(() => {
    setShow(!show)
  }, [show])

  return (
    <FormControl>
      <FormLabel htmlFor="password" id="password-label">
        {t('user.password_label')}
      </FormLabel>
      <InputGroup size="md">
        <Input
          autoComplete="new-password"
          type={show ? 'text' : 'password'}
          id="password"
          name="password"
          placeholder={t('user.password_placeholder')}
          aria-describedby="password-label"
          onChange={onChange}
          value={value}
          isRequired
        />
        <InputRightElement width="2.75rem">
          <IconButton
            size="sm"
            h="1.75rem"
            icon={show ? FaEyeSlash : FaEye}
            onClick={onClick}
            aria-label={show ? t('general.hide') : t('general.show')}
          />
        </InputRightElement>
      </InputGroup>
    </FormControl>
  )
}

InputPassword.propTypes = {
  onChange: PropTypes.func.isRequired,
  value: PropTypes.string.isRequired,
}
