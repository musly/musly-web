import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { history } from 'rooter'
import { useHistory } from 'rooter/react'
import { auth, persistence, firestore } from '../../../utils/firebase'
import { storage } from '../../../utils/Storage'
import { t } from '../../../utils/i18n'
import { getGroupLogger } from '../../../utils/logger'
import { LoadingContext } from '../../../components/LoadingProvider'
import { UserContext } from './UserContext'
import * as constants from '../../constants'

const { log, logResponse, logError } = getGroupLogger('User', '#e38d24', '#e36724')

const defaultUser = {
  id: null,
  email: null,
  emailVerified: null,
  displayName: null,
  isLoggedIn: false,
  birthday: null,
  gender: null,
}
export const authRoutes = [
  constants.PATH_LOGIN,
  constants.PATH_SIGNUP,
]

/**
 * Handles the user context, login, logout, signup, update and auth protection.
 */
class UserProviderBase extends PureComponent {
  constructor(props) {
    super(props)

    this.collection = firestore.collection('users')
    this.state = {
      // Get the current user from localStorage or use default user.
      user: storage.getItem('currentUser', defaultUser),
      initialized: false,
    }
  }

  componentDidMount() {
    this.setState({ initialized: true })
  }

  componentDidUpdate() {
    if (!this.state.initialized) {
      return
    }

    const current = window.location.pathname
    let replaceData

    // If user is not logged in, and not navigating to an auth route, redirect to login.
    if (!this.state.user.isLoggedIn && !authRoutes.includes(current)) {
      replaceData = {
        to: constants.PATH_LOGIN,
        meta: {
          redirect: current,
        },
      }

    // If the user is logged in, but is on an auth route, redirect to previous target or index.
    } else if (this.state.user.isLoggedIn && authRoutes.includes(current)) {
      if (this.props.route.meta.redirect) {
        replaceData = {
          to: this.props.route.meta.redirect,
        }
      } else {
        replaceData = {
          to: '/',
        }
      }
    }

    if (replaceData) {
      log('historyReplace', replaceData)
      history.replace(replaceData)
    }
  }

  get userContext() {
    return {
      user: this.state.user,
      login: this.login,
      signup: this.signup,
    }
  }

  /**
   * Performs the user login action.
   * @param {Object} credentials The user credentials.
   * @param {string} credentials.email  The user email address.
   * @param {string} credentials.password The user password.
   * @param {boolean} credentials.rememberMe Whether the user login should be persisted.
   */
  login = async (credentials) => {
    log('login', { credentials })

    if (!credentials || !credentials.email || !credentials.password) {
      throw new Error(t('errors.user_credentials_missing'))
    }

    this.context.setIsLoading(true)

    if (credentials.rememberMe) {
      // Persist the login state locally.
      await auth.setPersistence(persistence.LOCAL)
    } else {
      // Do not persist the login state.
      await auth.setPersistence(persistence.NONE)
    }

    try {
      const { user } = await auth.signInWithEmailAndPassword(
        credentials.email,
        credentials.password
      )
      const snapshot = await this.collection.doc(user.uid).get()

      this.setState({ user: this.buildUser(user, snapshot) }, () => {
        logResponse('login', { user: this.state.user })
        if (credentials.rememberMe) {
          storage.setItem('currentUser', this.state.user)
        }
      })
    } catch (error) {
      logError('login', error)
    } finally {
      this.context.setIsLoading(false)
    }
  }

  /**
   * Performs the user signup action.
   * @param {Object} credentials The user credentials.
   * @param {string} credentials.email  The user email address.
   * @param {string} credentials.password The user password.
   * @param {string} credentials.passwordConfirm The password confirmation.
   * It should be equal to the password!
   */
  signup = async (credentials) => {
    log('signup', credentials)

    if (
      !credentials
      || !credentials.email
      || !credentials.password
      || !credentials.passwordConfirm
    ) {
      throw new Error(t('errors.user_credentials_missing'))
    }

    this.context.setIsLoading(true)

    // Do not persist the login state on sign up!
    await auth.setPersistence(persistence.NONE)

    try {
      const { user } = await auth.createUserWithEmailAndPassword(
        credentials.email,
        credentials.password
      )
      const ref = this.collection.doc(user.uid)
      await this.collection.doc(user.uid).set({
        birthday: credentials.birthday || null,
        gender: credentials.gender || null,
        displayName: credentials.displayName || null,
      })
      const snapshot = await ref.get()

      this.setState({ user: this.buildUser(user, snapshot) }, () => {
        logResponse('signup', { user: this.state.user })
      })
    } catch (error) {
      logError('signup', error)
    } finally {
      this.context.setIsLoading(false)
    }
  }

  /**
   * Creates the output user object.
   * @param {firebase.User} user The user.
   * @param {firebase.firestore.DocumentSnapshot} snapshot The user data.
   * @returns {Object} The user object.
   */
  buildUser = (user, snapshot) => ({
    ...defaultUser,
    id: user.uid,
    email: user.email,
    emailVerified: user.emailVerified,
    displayName: user.displayName,
    isLoggedIn: user.isLoggedIn || true,
    ...(snapshot.data() || {}),
  })

  render() {
    return (
      <UserContext.Provider value={this.userContext}>
        {this.props.children}
      </UserContext.Provider>
    )
  }
}

UserProviderBase.contextType = LoadingContext

UserProviderBase.propTypes = {
  children: PropTypes.node.isRequired,
  route: PropTypes.shape().isRequired,
}

/**
 * Provides the user context that handles login, logout, signup, update, and auth protection.
 * @param {Object} props The component props.
 * @property {ReactNode} props.children The component children.
 * @returns {JSX.Element}
 */
export function UserProvider({ children }) {
  const { current } = useHistory()

  return (
    <UserProviderBase route={current}>
      {children}
    </UserProviderBase>
  )
}

UserProvider.propTypes = {
  children: PropTypes.node.isRequired,
}
