import React from 'react'
import PropTypes from 'prop-types'
import { FormControl, FormLabel, Input } from '@chakra-ui/core'
import { t } from '../../../utils/i18n'

export function InputEmail({ onChange, value }) {
  return (
    <FormControl>
      <FormLabel htmlFor="email" id="email-label">
        {t('user.email_label')}
      </FormLabel>
      <Input
        autoComplete="new-password"
        type="email"
        id="email"
        name="email"
        placeholder={t('user.email_placeholder')}
        aria-describedby="email-label"
        onChange={onChange}
        value={value}
        isRequired
      />
    </FormControl>
  )
}

InputEmail.propTypes = {
  onChange: PropTypes.func.isRequired,
  value: PropTypes.string.isRequired,
}
