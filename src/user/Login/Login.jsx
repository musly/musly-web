import React, { useCallback, memo } from 'react'
import { Box, Flex, Image, Text, Checkbox, Button } from '@chakra-ui/core'
import { t } from '../../utils/i18n'
import { View, LinkButton, Link, Copyright } from '../../components'
import { useFormState, useLoadingState } from '../../hooks'
import { useUser } from '../hooks'
import { InputEmail, InputPassword } from '../components'

const initialState = {
  email: '',
  password: '',
  rememberMe: false,
}

const Login = memo(function Login() {
  const { login } = useUser()
  const { isLoading } = useLoadingState()

  const onComplete = useCallback((values) => {
    // TODO: Build in error handling
    login(values)
  }, [login])

  const { values, onChange, onSubmit } = useFormState(initialState, onComplete)

  return (
    <View title={t('user.login_title')}>
      <Box w="full" maxW="sm" mx="auto" pt={20}>
        <Image src="/assets/logo-black.svg" mx="auto" mb={8} w={48} h="auto" />
        <Text textAlign="center" fontSize="lg" mb={5}>
          {t('user.login_headline')}
        </Text>
        <Box as="form" bg="white" shadow="lg" rounded="lg" p={10} mb={6} onSubmit={onSubmit}>
          <Box mb={4}>
            <InputEmail value={values.email} onChange={onChange} />
          </Box>
          <Box mb={4}>
            <InputPassword value={values.password} onChange={onChange} />
          </Box>
          <Box mb={6}>
            <Checkbox name="rememberMe" onChange={onChange} isChecked={values.rememberMe}>
              {t('user.rememberme_label')}
            </Checkbox>
          </Box>
          <Flex align="center" justify="center">
            <Button type="submit" mr={4} flexGrow={1} variantColor="yellow" isLoading={isLoading}>
              {t('user.login_btn')}
            </Button>
            <LinkButton href="/forgot-password" variant="outline" isLoading={isLoading}>
              {t('user.forgotpassword_link')}
            </LinkButton>
          </Flex>
        </Box>
        <Text textAlign="center" mb={4}>
          <Box as="span" userSelect="none" color="gray.600">
            {t('user.no_account')}
          </Box>
          {' '}
          <Link href="/signup">
            {t('user.signup_link')}
          </Link>
        </Text>
        <Box textAlign="center">
          <Copyright />
        </Box>
      </Box>
    </View>
  )
})

export default Login
