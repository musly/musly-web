import { theme } from '@chakra-ui/core'

export default {
  ...theme,
  fonts: {
    ...theme.fonts,
    body: 'Roboto, sans-serif',
    heading: 'Roboto, sans-serif',
  },
  colors: {
    ...theme.colors,
    yellow: {
      ...theme.colors.yellow,
      500: '#FFC136',
    },
  },
}
