import React from 'react'
import { BsCardList } from 'react-icons/bs'
import { Box } from '@chakra-ui/core'

export function SetlistIcon(props) {
  return (
    <Box as={BsCardList} {...props} />
  )
}
