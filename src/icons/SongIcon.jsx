import React from 'react'
import { GiMusicalNotes } from 'react-icons/gi'
import { Box } from '@chakra-ui/core'

export function SongIcon(props) {
  return (
    <Box as={GiMusicalNotes} {...props} />
  )
}
