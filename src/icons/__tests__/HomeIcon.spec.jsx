import React from 'react'
import { shallow } from 'enzyme'
import { HomeIcon } from '../HomeIcon'

describe('<HomeIcon />', () => {
  it('should render', () => {
    const wrapper = shallow(
      <HomeIcon />
    )

    expect(wrapper).toMatchSnapshot()
  })
})
