import React from 'react'
import { shallow } from 'enzyme'
import { SongIcon } from '../SongIcon'

describe('<SongIcon />', () => {
  it('should render', () => {
    const wrapper = shallow(
      <SongIcon />
    )

    expect(wrapper).toMatchSnapshot()
  })
})
