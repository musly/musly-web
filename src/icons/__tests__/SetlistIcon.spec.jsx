import React from 'react'
import { shallow } from 'enzyme'
import { SetlistIcon } from '../SetlistIcon'

describe('<SetlistIcon />', () => {
  it('should render', () => {
    const wrapper = shallow(
      <SetlistIcon />
    )

    expect(wrapper).toMatchSnapshot()
  })
})
