import React from 'react'
import { shallow } from 'enzyme'
import { OverviewIcon } from '../OverviewIcon'

describe('<OverviewIcon />', () => {
  it('should render', () => {
    const wrapper = shallow(
      <OverviewIcon />
    )

    expect(wrapper).toMatchSnapshot()
  })
})
