import React from 'react'
import { FaTachometerAlt } from 'react-icons/fa'
import { Box } from '@chakra-ui/core'

export function HomeIcon(props) {
  return (
    <Box as={FaTachometerAlt} {...props} />
  )
}
