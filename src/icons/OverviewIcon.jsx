import React from 'react'
import { FaBars } from 'react-icons/fa'
import { Box } from '@chakra-ui/core'

export function OverviewIcon(props) {
  return (
    <Box as={FaBars} {...props} />
  )
}
