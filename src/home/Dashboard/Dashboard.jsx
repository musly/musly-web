import React from 'react'
import { SimpleGrid } from '@chakra-ui/core'
import { View, ViewHeader } from '../../components'
import { useUser } from '../../user/hooks'
import { MusicWidget } from '../../music/components'
import { t } from '../../utils/i18n'

/**
 * Renders the main dashboard that a user sees after login.
 * @returns {JSX.Element}
 */
export function Dashboard() {
  const { user } = useUser()
  const welcomeMessage = user.displayName ? t('user.welcome_personal', { name: user.displayName }) : t('user.welcome')

  return (
    <View>
      <ViewHeader title={welcomeMessage} />
      <SimpleGrid columns={4} gap={6}>
        <MusicWidget />
      </SimpleGrid>
    </View>
  )
}
