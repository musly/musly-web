import React from 'react'
import { render } from 'react-dom'
import { HelmetProvider } from 'react-helmet-async'
import * as Sentry from '@sentry/browser'
import { i18n } from './utils/i18n'
import { registerServiceWorker, unregisterServiceWorker } from './utils/service-worker'
import { ErrorBoundary } from './components'
import App from './App'

if (process.env.NODE_ENV === 'production') {
  // Init Sentry.io
  Sentry.init({
    dsn: 'https://66f861ec2c274fdf97f3c1fbe832c64b@o411488.ingest.sentry.io/5286742',
    environment: process.env.SENTRY_ENVIRONMENT,
  })
  // Init service worker => offline caching
  registerServiceWorker()
} else {
  unregisterServiceWorker()
}

i18n.init()

render(
  <ErrorBoundary>
    <HelmetProvider>
      <App />
    </HelmetProvider>
  </ErrorBoundary>,
  document.getElementById('root')
)
