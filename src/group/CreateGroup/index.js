import React from 'react'

const CreateGroup = React.lazy(() => import(/* webpackChunkName: "create-group" */'./CreateGroup'))

export { CreateGroup }
