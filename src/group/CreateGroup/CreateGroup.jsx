import React, { memo } from 'react'
import { Box, Image } from '@chakra-ui/core'
import { t } from '../../utils/i18n'
import { View } from '../../components'

const CreateGroup = memo(function CreateGroup() {
  return (
    <View title={t('group.create_title')}>
      <Box w="full" maxW="sm" mx="auto" pt={20}>
        <Image src="/assets/logo-black.svg" mx="auto" mb={8} w={48} h="auto" />
      </Box>
    </View>
  )
})

export default CreateGroup
