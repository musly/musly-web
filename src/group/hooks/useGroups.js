import { useContext } from 'react'
import { GroupContext } from '../components/GroupProvider/GroupContext'

export function useGroups() {
  return useContext(GroupContext)
}
