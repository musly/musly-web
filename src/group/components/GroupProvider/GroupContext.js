import { createContext } from 'react'

export const GroupContext = createContext({
  currentGroup: null,
  groups: [],
})
