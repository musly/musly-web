import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { history } from 'rooter'
import { useHistory } from 'rooter/react'
import { GroupContext } from './GroupContext'
import { firestore } from '../../../utils/firebase'
import { getGroupLogger } from '../../../utils/logger'
import { UserContext } from '../../../user/components/UserProvider/UserContext'
import { authRoutes } from '../../../user/components/UserProvider/UserProvider'

const { log, logResponse, logError } = getGroupLogger('Group', '#e38d24', '#e36724')

const defaultGroup = {
  id: null,
  title: null,
  type: null,
}

class GroupProviderBase extends PureComponent {
  constructor(props) {
    super(props)

    this.collection = firestore.collection('groups')
    this.referenceCollection = firestore.collection('users_groups')
    this.state = {
      currentGroup: null,
      groups: [],
    }
  }

  async componentDidUpdate() {
    const current = window.location.pathname

    // Don't do anything if the user is not logged in
    // or still on one of the auth routes.
    if (
      !this.context.user
      || !this.context.user.isLoggedIn
      || authRoutes.includes(current)
      || current === '/create-group'
    ) {
      return
    }

    // If the user doesn't have any group, we asume it's a fresh signup
    // and redirect him to the group creation screen.
    if (!this.context.user.groups || this.context.user.groups.length === 0) {
      const replaceData = { to: '/create-group', meta: { redirect: current } }
      log('historyReplace', replaceData)
      history.replace(replaceData)
      return
    }

    const usersGroups = await this.referenceCollection
      .where('groupId', 'in', this.context.user.groups)
      .get()

    if (usersGroups.docs.length === 0) {
      return
    }

    try {
      await firestore.runTransaction((transaction) => {
        log('getUserGroups', this.context.user.groups)

        const groups = []
        usersGroups.forEach(async ({ uid }) => {
          const docRef = this.collection.doc(uid)
          const snapshot = await transaction.get(docRef)

          if (!snapshot.exists) {
            return
          }

          groups.push(this.buildGroup(snapshot))
        })

        const result = { groups, currentGroup: groups[0].id }
        logResponse('getUserGroups', result)
        this.setState(result)
      })
    } catch (error) {
      logError('getUserGroups', { error })
    }
  }

  get groupContext() {
    return {
      ...this.state,
    }
  }

  buildGroup = (snapshot) => ({
    ...defaultGroup,
    id: snapshot.uid,
    ...snapshot.data(),
  })

  render() {
    return (
      <GroupContext.Provider value={this.groupContext}>
        {this.props.children}
      </GroupContext.Provider>
    )
  }
}

GroupProviderBase.contextType = UserContext

GroupProviderBase.propTypes = {
  children: PropTypes.node.isRequired,
}

export function GroupProvider({ children }) {
  const { current } = useHistory()

  return (
    <GroupProviderBase route={current}>
      {children}
    </GroupProviderBase>
  )
}

GroupProvider.propTypes = {
  children: PropTypes.node.isRequired,
}
