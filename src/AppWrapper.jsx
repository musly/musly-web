import React from 'react'
import PropTypes from 'prop-types'
import { ThemeProvider, CSSReset } from '@chakra-ui/core'
import theme from './App.theme'
import { Viewport } from './components'
import { useUser } from './user/hooks'
import { useGroups } from './group/hooks'

export function AppWrapper({ children }) {
  const { user } = useUser()
  const { groups } = useGroups()

  return (
    <ThemeProvider theme={theme}>
      <CSSReset />
      <Viewport pt={(user.isLoggedIn && groups.length > 0) && '50px'}>
        {children}
      </Viewport>
    </ThemeProvider>
  )
}

AppWrapper.propTypes = {
  children: PropTypes.node.isRequired,
}
