import MessageFormat from 'messageformat'
import Messages from 'messageformat/messages'
import { getGroupLogger } from './logger'
import { locales } from '../locales'

const lang = process.env.LOCALE || 'en'
const { log } = getGroupLogger('i18n', '#3be3db')

/**
 * I18n handels localization. It initializes the correct locales and provides the
 * translation mechanism. To initialize new translations, call the `init()` method and
 * pass it the translations strings object as an argument.
 */
class I18n {
  /**
   * Initializes the translation mechanism and loads the needed locales.
   * @param {Object} locales The translation strings object.
   */
  init() {
    this.messageformat = new MessageFormat(lang)
    this.messages = new Messages(this.messageformat.compile({
      [lang]: locales[lang],
    }))
    this.messages.locale = lang

    log('initiated', { lang })
  }

  /**
   * Takes an input JSON path string and transforms it into a translated string.
   * @param {string} key A string representing a JSON path, that locates the translation string.
   * @param {Object} [data] An object containing any data that is needed evaluated
   * into the translation string.
   * @param {string} [defaultOutput] Some default output string that will be used, if a
   * translation could not be found at the specified path.
   * @returns {string}
   */
  translate = (key, data = undefined, defaultOutput = null) => {
    const pieces = key.split('.')
    const query = pieces.length > 1 ? pieces : pieces[0]

    const output = this.messages.hasObject(query) ? key : this.messages.get(query, data)

    if (output === key) {
      if (!defaultOutput) {
        return key
      }

      return defaultOutput
    }

    return output
  }
}

export const i18n = new I18n()

/**
 * Takes an input JSON path string and transforms it into a translated string.
 * @param {string} key A string representing a JSON path, that locates the translation string.
 * @param {Object} [data] An object containing any data that is needed evaluated
 * into the translation string.
 * @param {string} [defaultOutput] Some default output string that will be used, if a
 * translation could not be found at the specified path.
 * @returns {string}
 */
export function t(key, data = undefined, defaultOutput = null) {
  return i18n.translate(key, data, defaultOutput)
}
