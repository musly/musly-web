import { isNumeric } from '../validation'

describe('Utils | validation', () => {
  describe('isNumeric', () => {
    it('should treat numeric number as numeric', () => {
      expect(isNumeric('123')).toBe(true)
    })

    it('should treat number as numeric', () => {
      expect(isNumeric(123)).toBe(true)
    })

    it('should treat anything else as non-numeric', () => {
      expect(isNumeric('abc')).toBe(false)
      expect(isNumeric([])).toBe(false)
    })
  })
})
