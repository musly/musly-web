import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/firestore'
import { getGroupLogger } from './logger'

const { log, logError } = getGroupLogger('Firebase', '#fcba03')

/**
 * Logs a firebase action and it's corresponding data. It will log an error, if an error object
 * is passed as third parameter.
 * @param {string} action The firebase action name.
 * @param {Object} [data] The data that has been used in the action.
 * @param {Error|null} [error=null] A potential error hat was caused by the action.
 */
export function logFirebase(action, data = {}, error = null) {
  if (error !== null) {
    logError(action, error)
    return
  }

  log(action, data)
}

const firebaseConfig = {
  apiKey: process.env.FIREBASE_API_KEY,
  authDomain: process.env.FIREBASE_AUTH_DOMAIN,
  databaseURL: process.env.FIREBASE_DATABASE_URL,
  projectId: process.env.FIREBASE_PROJECT_ID,
  storageBucket: process.env.FIREBASE_STORGAE_BUCKET,
  messagingSenderId: process.env.FIREBASE_MESSAGING_SENDER_ID,
  appId: process.env.FIREBASE_APP_ID,
  measurementId: process.env.FIREBASE_MEASUREMENT_ID,
}

const app = firebase.initializeApp(firebaseConfig)
export const auth = app.auth()
export const persistence = firebase.auth.Auth.Persistence
export const firestore = app.firestore()

logFirebase('initiated')

/**
 * Performs a batch update on multiple documents.
 * @param {firebase.firestore.CollectionReference} collection The collection.
 * @param {string[]} docIds IDs of documents to batch update.
 * @param {Obect} data The data to be updated.
 * @returns {Promise<void>} A promise that resolves when the batch update is done.
 */
export function batchUpdate(collection, docIds, data) {
  const batch = firestore.batch()

  docIds.forEach((id) => {
    batch.update(collection.doc(id), data)
  })

  return batch.commit()
}

/**
 * Deletes multiple documents in a batch transaction.
 * @param {firebase.firestore.CollectionReference} collection The collection.
 * @param {string[]} docIds IDs of documents to batch update.
 * @returns {Promise<void>} A promise that resolves when the batch delete is done.
 */
export function batchDelete(collection, docIds) {
  const batch = firestore.batch()

  docIds.forEach((id) => {
    batch.delete(collection.doc(id))
  })

  return batch.commit()
}
