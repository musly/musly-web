import store from 'store'
import { getGroupLogger } from './logger'

const STORAGE_KEY = 'musly'

const { log, logError } = getGroupLogger('Storage:%t', '#1f9ecc', '#1f75cc')

/**
 * Storage handles add, retrieving or removing an item in the store.
 */
export class Storage {
  store = store;

  /**
   * Retrieves an item from the store.
   * @param {string} key The item key.
   * @param {Object|null} [defaultValue] A default value that will be returned.
   * @returns {Object|null} The item value or a default. Will return `null` if the item was
   * not found and no default was set.
   */
  getItem(key, defaultValue = null) {
    try {
      const state = this.store.get(STORAGE_KEY)
      const value = (state && state[key]) ? state[key] : defaultValue
      log(`get:${key}`, { value })
      return value
    } catch (error) {
      logError(`get:${key}`, { error })
      return null
    }
  }

  /**
   * Add an item to the store.
   * @param {string} key The key to store the item under.
   * @param {Object} data The data that needs to be stored under the key.
   */
  setItem(key, data) {
    try {
      const state = this.store.get(STORAGE_KEY)
      store.set(STORAGE_KEY, {
        ...state,
        [key]: data,
      })
      log(`set:${key}`, { data })
    } catch (error) {
      logError(`set:${key}`, { error })
    }
  }

  /**
   * Deletes an item from the store.
   * @param {string} key The key of the item.
   */
  removeItem(key) {
    const state = this.store.get(STORAGE_KEY)
    const { [key]: removed, ...rest } = state
    store.set(STORAGE_KEY, rest)
    log(`delete:${key}`, { key })
  }
}

export const storage = new Storage()
