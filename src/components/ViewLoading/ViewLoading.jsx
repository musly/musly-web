import React from 'react'
import PropTypes from 'prop-types'
import { Flex, Spinner } from '@chakra-ui/core'
import { useLoadingState } from '../../hooks'

export function ViewLoading({ show }) {
  const { isViewLoading } = useLoadingState()

  if (isViewLoading || show) {
    return (
      <Flex as="article" flex="none" flexGrow={1} h="full" alignItems="center" justifyContent="center">
        <Spinner thickness="6px" speed="650ms" size="xl" color="blue.500" />
      </Flex>
    )
  }

  return null
}

ViewLoading.propTypes = {
  show: PropTypes.bool,
}

ViewLoading.defaultProps = {
  show: false,
}
