import React from 'react'
import { Text } from '@chakra-ui/core'
import { t } from '../../utils/i18n'

export function Copyright() {
  const year = React.useMemo(() => {
    const current = new Date().getFullYear()
    if (current > 2019) {
      return `2019-${current}`
    }

    return current
  }, [])
  return (
    <Text color="gray.400" fontSize="xs">
      <span
        dangerouslySetInnerHTML={{
          __html: t('general.copyright', { year }),
        }}
      />
    </Text>
  )
}
