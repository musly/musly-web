import React from 'react'
import { shallow } from 'enzyme'
import { Sheet } from '../Sheet'

describe('<Sheet />', () => {
  it('should render', () => {
    const wrapper = shallow(
      <Sheet />
    )

    expect(wrapper).toMatchSnapshot()
  })
})
