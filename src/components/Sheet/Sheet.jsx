import React from 'react'
import PropTypes from 'prop-types'
import { Box } from '@chakra-ui/core'

export function Sheet({ children, ...rest }) {
  return (
    <Box bg="white" shadow="md" p={4} rounded="md" {...rest}>
      {children}
    </Box>
  )
}

Sheet.propTypes = {
  children: PropTypes.node,
}

Sheet.defaultProps = {
  children: null,
}
