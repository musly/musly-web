import React from 'react'
import PropTypes from 'prop-types'
import { Box } from '@chakra-ui/core'

export function ViewSection({ children, isBig }) {
  return (
    <Box as="section" py={isBig ? 6 : 4}>
      {children}
    </Box>
  )
}

ViewSection.propTypes = {
  children: PropTypes.node.isRequired,
  isBig: PropTypes.bool,
}

ViewSection.defaultProps = {
  isBig: false,
}
