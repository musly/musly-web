import React from 'react'
import PropTypes from 'prop-types'
import { Box } from '@chakra-ui/core'

export function ViewContent({ children, gap, row }) {
  const padding = gap ? 4 : 0
  const flow = row ? 'row' : 'column'

  return (
    <Box as="article" d="flex" overflow="auto" flexGrow={1} p={padding} flexDirection={flow}>
      {children}
    </Box>
  )
}

ViewContent.propTypes = {
  children: PropTypes.node.isRequired,
  gap: PropTypes.bool,
  row: PropTypes.bool,
}

ViewContent.defaultProps = {
  gap: false,
  row: false,
}
