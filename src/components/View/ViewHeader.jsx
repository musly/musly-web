import React from 'react'
import PropTypes from 'prop-types'
import { Flex, Text, Box } from '@chakra-ui/core'

export function ViewHeader({ title, children }) {
  return (
    <Flex as="header" aria-label="page caption" h={16} align="center">
      <Text as="h1" fontSize="lg" fontWeight="medium">
        {title}
      </Text>
      <Box flexGrow={1} />
      {children}
    </Flex>
  )
}

ViewHeader.propTypes = {
  title: PropTypes.string.isRequired,
  children: PropTypes.node,
}

ViewHeader.defaultProps = {
  children: null,
}
