import React from 'react'
import PropTypes from 'prop-types'
import { Helmet } from 'react-helmet-async'
import { Flex } from '@chakra-ui/core'

export function View({ children, title, sidebar: Sidebar }) {
  const htmlTitle = React.useMemo(() => (
    title ? `${title} :: musly` : 'musly'
  ), [title])

  return (
    <React.Fragment>
      <Helmet>
        <title>{htmlTitle}</title>
      </Helmet>
      {Sidebar && (
        <Sidebar />
      )}
      <Flex as="article" flex="none" overflow="auto" px={5} flexGrow={1} flexDirection="column">
        {children}
      </Flex>
    </React.Fragment>
  )
}

View.propTypes = {
  children: PropTypes.node.isRequired,
  sidebar: PropTypes.func,
  title: PropTypes.string,
}

View.defaultProps = {
  title: null,
  sidebar: null,
}
