import React from 'react'
import PropTypes from 'prop-types'
import { history } from 'rooter'
import { useHistory } from 'rooter/react'
import { Link } from '@chakra-ui/core'

export function NavigationItem({ title, href }) {
  const { current: { pathname } } = useHistory()
  const isActive = (href === '/') ? (pathname === href) : pathname.startsWith(href)
  const color = isActive ? 'gray.900' : 'gray.300'
  const hoverColor = isActive ? 'gray.900' : 'white'
  const props = {
    ...isActive && {
      bg: 'yellow.500',
    },
  }

  const handleClick = React.useCallback((event) => {
    event.preventDefault()

    if (!isActive) {
      history.push({ to: href })
    }
  }, [href, isActive])

  return (
    <Link href={href} color={color} _hover={{ color: hoverColor }} _focus={{ boxShadow: 'none' }} onClick={handleClick} px={5} mr={2} h="full" d="flex" alignItems="center" {...props}>
      {title}
    </Link>
  )
}

NavigationItem.propTypes = {
  href: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
}
