import React from 'react'
import { Flex } from '@chakra-ui/core'
import { NavigationItem } from './NavigationItem'

export function Navigation() {
  return (
    <Flex as="nav" alignItems="stretch">
      <NavigationItem href="/" title="Home" />
      <NavigationItem href="/music" title="Music" />
      <NavigationItem href="/group" title="Group" />
    </Flex>
  )
}
