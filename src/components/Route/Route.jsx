import React from 'react'
import PropTypes from 'prop-types'
import { Route as BaseRoute } from 'rooter/react'

export function Route({ path, component: Component }) {
  return (
    <BaseRoute path={path}>
      <Component />
    </BaseRoute>
  )
}

Route.propTypes = {
  ...BaseRoute.propTypes,
  component: PropTypes.elementType,
}

Route.defaultProps = BaseRoute.defaultProps
