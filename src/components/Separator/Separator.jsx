import React, { memo } from 'react'
import PropTypes from 'prop-types'
import { Box, Flex } from '@chakra-ui/core'
import { t } from '../../utils/i18n'

export const Separator = memo(function Separator({ label, dense, light }) {
  return (
    <Flex borderTop="1px" borderColor={light ? 'gray.200' : 'gray.300'} justify="center" w="full" my={!dense && 4}>
      {label && (
        <Box bg="white" px={2} color={light ? 'gray.300' : 'gray.400'} fontStyle="italic" userSelect="none" mt="-8px" lineHeight={1}>
          {t('general.or')}
        </Box>
      )}
    </Flex>
  )
})

Separator.propTypes = {
  dense: PropTypes.bool,
  label: PropTypes.bool,
  light: PropTypes.bool,
}

Separator.defaultProps = {
  dense: false,
  label: false,
  light: false,
}
