import React from 'react'
import PropTypes from 'prop-types'
import { Flex } from '@chakra-ui/core'

/**
 * Renders the `<main>` viewport element.
 * @param {Object} props The component props.
 * @property {ReactNode} children The component children.
 * @returns {JSX.Element}
 */
export function Viewport({ children, ...rest }) {
  return (
    <Flex
      as="main"
      h="100vh"
      w="100vw"
      minW="1200px"
      bg="gray.100"
      pos="relative"
      zIndex={1}
      {...rest}
    >
      {children}
    </Flex>
  )
}

Viewport.propTypes = {
  children: PropTypes.node.isRequired,
}
