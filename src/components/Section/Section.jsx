import React from 'react'
import PropTypes from 'prop-types'
import { Box } from '@chakra-ui/core'

export function Section({ children, isBig, ...rest }) {
  return (
    <Box as="section" py={2} fontSize={isBig && 'lg'} {...rest}>
      {children}
    </Box>
  )
}

Section.propTypes = {
  children: PropTypes.node.isRequired,
  isBig: PropTypes.bool,
}

Section.defaultProps = {
  isBig: false,
}
