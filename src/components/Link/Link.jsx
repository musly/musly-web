import React, { useCallback } from 'react'
import { history } from 'rooter'
import { Link as BaseLink } from '@chakra-ui/core'

export function Link({ href, children, ...rest }) {
  const onClick = useCallback((event) => {
    event.preventDefault()
    history.push({ to: href })
  }, [href])

  return (
    <BaseLink href={href} onClick={onClick} color="blue.500" {...rest}>
      {children}
    </BaseLink>
  )
}

Link.propTypes = BaseLink.propTypes
Link.defaultProps = BaseLink.defaultProps
