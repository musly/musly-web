import React from 'react'
import PropTypes from 'prop-types'
import { Box } from '@chakra-ui/core'

export function Header({ children }) {
  return (
    <Box as="header" role="banner" height="50px" d="flex" alignItems="center" pos="fixed" width="100%" minW="1200px" zIndex={2} top="0" bg="blue.900" color="gray.100" borderBottom="3px solid" borderBottomColor="yellow.500">
      {children}
    </Box>
  )
}

Header.propTypes = {
  children: PropTypes.node.isRequired,
}
