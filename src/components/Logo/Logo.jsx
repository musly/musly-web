import React from 'react'
import { Box, Image } from '@chakra-ui/core'

export function Logo() {
  return (
    <Box d="flex" alignItems="center" maxHeight="20px">
      <Image src="/assets/icon.svg" mr={1} />
      <Image src="/assets/slug.svg" />
    </Box>
  )
}
