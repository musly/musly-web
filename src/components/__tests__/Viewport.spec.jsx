import React from 'react'
import { shallow } from 'enzyme'
import { Viewport } from '../Viewport'

describe('<Viewport />', () => {
  it('should render', () => {
    const wrapper = shallow(
      <Viewport>
        Some content
      </Viewport>
    )

    expect(wrapper).toMatchSnapshot()
  })
})
