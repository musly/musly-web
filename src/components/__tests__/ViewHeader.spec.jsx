import React from 'react'
import { shallow } from 'enzyme'
import { ViewHeader } from '../View/ViewHeader'

describe('<ViewHeader />', () => {
  it('should render', () => {
    const wrapper = shallow(
      <ViewHeader title="Some heade title">
        <div>Some content</div>
      </ViewHeader>
    )

    expect(wrapper).toMatchSnapshot()
  })
})
