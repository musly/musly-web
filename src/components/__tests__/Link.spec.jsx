import React from 'react'
import { shallow } from 'enzyme'
import { history } from 'rooter'
import { act } from 'react-test-renderer'
import { Link } from '../Link'

jest.mock('rooter', () => ({
  ...jest.requireActual('rooter'),
  history: {
    push: jest.fn(),
  },
}))

describe('<Link />', () => {
  it('should render', () => {
    let wrapper
    act(() => {
      wrapper = shallow(
        <Link href="/">
          Test content
        </Link>
      )
    })

    expect(wrapper).toMatchSnapshot()
  })

  it('should perform a history PUSH action', () => {
    let wrapper
    act(() => {
      wrapper = shallow(
        <Link href="/foo-bar">
          Test content
        </Link>
      )
    })

    expect(wrapper).toMatchSnapshot()
    const event = {
      preventDefault: jest.fn(),
    }
    wrapper.find('Link').simulate('click', event)
    expect(event.preventDefault).toHaveBeenCalled()
    expect(history.push).toHaveBeenCalledWith({ to: '/foo-bar' })
  })
})
