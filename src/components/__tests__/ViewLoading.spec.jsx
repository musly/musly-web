import React from 'react'
import { shallow } from 'enzyme'
import { ViewLoading } from '../ViewLoading'
import { useLoadingState } from '../../hooks'

jest.mock('../../hooks', () => ({
  ...jest.requireActual('../../hooks'),
  useLoadingState: jest.fn(),
}))

describe('<ViewLoading />', () => {
  it('should rnot render without "isViewLoading" or "show" prop', () => {
    useLoadingState.mockReturnValueOnce({ isViewLoading: false })

    const wrapper = shallow(
      <ViewLoading />
    )

    expect(wrapper).toMatchSnapshot()
    expect(wrapper.instance()).toEqual(null)
  })

  it('should render if "show" prop is set', () => {
    useLoadingState.mockReturnValueOnce({ isViewLoading: false })

    const wrapper = shallow(
      <ViewLoading show />
    )

    expect(wrapper).toMatchSnapshot()
    expect(wrapper.find('Flex')).toExist()
  })

  it('should render if "isViewLoading" is true', () => {
    useLoadingState.mockReturnValueOnce({ isViewLoading: true })

    const wrapper = shallow(
      <ViewLoading />
    )

    expect(wrapper).toMatchSnapshot()
  })
})
