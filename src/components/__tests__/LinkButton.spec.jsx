import React from 'react'
import { shallow } from 'enzyme'
import { history } from 'rooter'
import { act } from 'react-test-renderer'
import { LinkButton } from '../LinkButton'

jest.mock('rooter', () => ({
  ...jest.requireActual('rooter'),
  history: {
    push: jest.fn(),
  },
}))

describe('<LinkButton />', () => {
  it('should render', () => {
    let wrapper
    act(() => {
      wrapper = shallow(
        <LinkButton href="/">
          Test content
        </LinkButton>
      )
    })

    expect(wrapper).toMatchSnapshot()
  })

  it('should perform a history PUSH action', () => {
    let wrapper
    act(() => {
      wrapper = shallow(
        <LinkButton href="/foo-bar">
          Test content
        </LinkButton>
      )
    })

    const event = {
      preventDefault: jest.fn(),
    }
    wrapper.simulate('click', event)
    expect(event.preventDefault).toHaveBeenCalled()
    expect(history.push).toHaveBeenCalledWith({ to: '/foo-bar', meta: null })
  })
})
