import React from 'react'
import { shallow } from 'enzyme'
import { Separator } from '../Separator'

describe('<Separator />', () => {
  it('should render without label', () => {
    const wrapper = shallow(
      <Separator />
    )

    expect(wrapper).toMatchSnapshot()
  })

  it('should render with label', () => {
    const wrapper = shallow(
      <Separator label />
    )

    expect(wrapper).toMatchSnapshot()
  })
})
