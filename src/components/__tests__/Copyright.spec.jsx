import React from 'react'
import { shallow } from 'enzyme'
import { act } from 'react-test-renderer'
import { Copyright } from '../Copyright'
import { i18n, t } from '../../utils/i18n'

jest.mock('../../utils/i18n', () => ({
  ...jest.requireActual('../../utils/i18n'),
  t: jest.fn(),
}))

describe('Common | Components | Copyright', () => {
  beforeAll(async () => {
    await i18n.init({ en: {} })
  })

  it('should render', () => {
    let wrapper
    act(() => {
      wrapper = shallow(<Copyright />)
    })
    expect(wrapper).toMatchSnapshot()
  })

  it('should return only one year', () => {
    jest.spyOn(Date.prototype, 'getFullYear').mockImplementation(() => 2019)
    act(() => {
      shallow(<Copyright />)
    })
    expect(t).toHaveBeenCalledWith('general.copyright', { year: 2019 })
  })
})
