import React from 'react'
import { shallow } from 'enzyme'
import { Route } from '../Route'

describe('<Route />', () => {
  it('should render', () => {
    const wrapper = shallow(
      <Route path="/some" component={() => null} />
    )

    expect(wrapper).toMatchSnapshot()
  })
})
