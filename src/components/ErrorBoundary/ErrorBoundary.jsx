import { Component } from 'react'
import PropTypes from 'prop-types'
import { captureException } from '@sentry/browser'
import { logger } from '../../utils/logger'

export class ErrorBoundary extends Component {
  constructor(props) {
    super(props)

    this.state = {
      hasError: false,
    }
  }

  static getDerivedStateFromError() {
    return { hasError: true }
  }

  componentDidUpdate() {
    if (this.state.hasError) {
      logger.error('Something went wrong!')
    }
  }

  componentDidCatch(error) {
    // Log the error to sentry
    captureException(error)
  }

  render() {
    return this.props.children
  }
}

ErrorBoundary.propTypes = {
  children: PropTypes.node.isRequired,
}
