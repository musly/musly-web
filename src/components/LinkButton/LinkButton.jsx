import React from 'react'
import PropTypes from 'prop-types'
import { history } from 'rooter'
import { Button } from '@chakra-ui/core'

export function LinkButton({ href, meta, children, ...rest }) {
  const onClick = React.useCallback((event) => {
    event.preventDefault()
    history.push({ to: href, meta })
  }, [href, meta])

  return (
    <Button onClick={onClick} {...rest}>
      {children}
    </Button>
  )
}

LinkButton.propTypes = {
  children: PropTypes.node.isRequired,
  href: PropTypes.string.isRequired,
  meta: PropTypes.shape(),
}

LinkButton.defaultProps = {
  meta: null,
}
