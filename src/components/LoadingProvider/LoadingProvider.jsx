import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { LoadingContext } from './LoadingContext'

export function LoadingProvider({ children }) {
  const [isLoading, setIsLoading] = useState(false)
  const [isViewLoading, setIsViewLoading] = useState(false)

  const context = {
    isLoading,
    isViewLoading,
    setIsLoading,
    setIsViewLoading,
  }

  return (
    <LoadingContext.Provider value={context}>
      {children}
    </LoadingContext.Provider>
  )
}

LoadingProvider.propTypes = {
  children: PropTypes.node.isRequired,
}
