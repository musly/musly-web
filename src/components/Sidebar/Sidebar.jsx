import React from 'react'
import PropTypes from 'prop-types'
import { Flex } from '@chakra-ui/core'

export function Sidebar({ children }) {
  return (
    <Flex as="nav" width="220px" direction="column">
      {children}
    </Flex>
  )
}

Sidebar.propTypes = {
  children: PropTypes.node.isRequired,
}
