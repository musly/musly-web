import React from 'react'
import PropTypes from 'prop-types'
import { history } from 'rooter'
import { useHistory } from 'rooter/react'
import { Flex, Link, Box } from '@chakra-ui/core'

export function SidebarItem({ title, href, icon: Icon }) {
  const { current: { pathname } } = useHistory()
  const isActive = (pathname === href)
  const iconProps = {
    size: '16px',
    ...isActive && { color: 'yellow.500' },
  }

  const handleClick = React.useCallback((event) => {
    event.preventDefault()

    if (!isActive) {
      history.push({ to: href })
    }
  }, [href, isActive])

  return (
    <Flex flexDirection="column">
      <Link href={href} color={isActive ? 'black' : 'gray.600'} _hover={{ color: isActive ? 'black' : 'gray.900' }} d="flex" alignItems="center" overflow="hidden" w="full" height={12} px={4} onClick={handleClick}>
        {Icon && (
          <Box as="span" w={5} flex="none" d="flex" justifyContent="center">
            <Icon {...iconProps} />
          </Box>
        )}
        <Box as="span" ml={3}>
          {title}
        </Box>
      </Link>
    </Flex>
  )
}

SidebarItem.propTypes = {
  title: PropTypes.string.isRequired,
  href: PropTypes.string,
  icon: PropTypes.func,
}

SidebarItem.defaultProps = {
  icon: null,
  href: null,
}
