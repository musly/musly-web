import { hot } from 'react-hot-loader/root'
import React from 'react'
import { Router } from 'rooter/react'
import { Login } from './user/Login'
import { Signup } from './user/Signup'
import { Profile } from './user/Profile'
import { GroupProvider } from './group/components'
import { CreateGroup } from './group/CreateGroup'
import { MusicOverview } from './music/MusicOverview'
import { ViewLoading, LoadingProvider, Route } from './components'
import { UserProvider } from './user/components'
import { AppHeader } from './AppHeader'
import { AppWrapper } from './AppWrapper'
import { Dashboard } from './home/Dashboard'

function App() {
  return (
    <Router>
      <LoadingProvider>
        <UserProvider>
          <GroupProvider>
            <AppWrapper>
              <AppHeader />
              <ViewLoading />
              <Route path="/login" component={Login} />
              <Route path="/signup" component={Signup} />
              {/* Auth protected routes */}
              <React.Suspense fallback={<ViewLoading show />}>
                <Route path="/" component={Dashboard} />
                <Route path="/create-group" component={CreateGroup} />
                <Route path="/music" component={MusicOverview} />
                <Route path="/profile" component={Profile} />
              </React.Suspense>
            </AppWrapper>
          </GroupProvider>
        </UserProvider>
      </LoadingProvider>
    </Router>
  )
}

export default hot(App)
