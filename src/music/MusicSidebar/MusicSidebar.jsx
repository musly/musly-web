import React from 'react'
import { Sidebar, SidebarItem } from '../../components'
import { OverviewIcon, SongIcon, SetlistIcon } from '../../icons'

export function MusicSidebar() {
  return (
    <Sidebar>
      <SidebarItem title="Overview" href="/music" icon={OverviewIcon} />
      <SidebarItem title="Songs" href="/music/songs" icon={SongIcon} />
      <SidebarItem title="Setlists" href="/music/setlists" icon={SetlistIcon} />
    </Sidebar>
  )
}
