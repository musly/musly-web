import React from 'react'
import { shallow } from 'enzyme'
import { MusicWidget } from '../MusicWidget'

describe('<MusicWidget />', () => {
  it('should render', () => {
    const wrapper = shallow(
      <MusicWidget />
    )

    expect(wrapper).toMatchSnapshot()
  })
})
