import React from 'react'
import { StatGroup, Stat, StatLabel, StatNumber } from '@chakra-ui/core'
import { Sheet } from '../../../components'

export function MusicWidget() {
  return (
    <Sheet userSelect="none">
      <StatGroup>
        <Stat>
          <StatLabel>Songs</StatLabel>
          <StatNumber>1056</StatNumber>
        </Stat>
        <Stat>
          <StatLabel>Setlists</StatLabel>
          <StatNumber>48</StatNumber>
        </Stat>
        <Stat>
          <StatLabel>Events</StatLabel>
          <StatNumber>22</StatNumber>
        </Stat>
      </StatGroup>
    </Sheet>
  )
}
