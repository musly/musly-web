import React from 'react'
import { Grid, Box } from '@chakra-ui/core'
import { View, ViewHeader, Sheet } from '../../components'
import { MusicSidebar } from '../MusicSidebar'

function MusicOverview() {
  return (
    <View sidebar={MusicSidebar}>
      <ViewHeader title="Music" />
      <Sheet mb={2}>
        This is some sheet content
      </Sheet>
      <Grid templateColumns="repeat(2, 1fr)" gap={4} my={2}>
        <Sheet>
          This is some sheet content
        </Sheet>
        <Sheet>
          This is some sheet content
        </Sheet>
      </Grid>
      <Box py={2}>
        Some Regular section here.
      </Box>
    </View>
  )
}

export default MusicOverview
