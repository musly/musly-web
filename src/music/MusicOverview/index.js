import React from 'react'

const MusicOverview = React.lazy(() => import(/* webpackChunkName: "music-overview" */'./MusicOverview'))

export { MusicOverview }
