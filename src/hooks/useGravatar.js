import React from 'react'
import querystring from 'query-string'
import isRetina from 'is-retina'
import md5 from 'md5'

const baseUrl = '//www.gravatar.com/avatar/'

export function useGravatar(email) {
  const formattedEmail = `${email}`.trim().toLowerCase()
  const hash = React.useMemo(() => md5(formattedEmail), [formattedEmail])
  const query = querystring.stringify({ s: 256, r: 'g', d: 'mp' })
  const retinaQuery = querystring.stringify({ s: 256 * 2, r: 'g', d: 'mp' })
  const src = `${baseUrl}${hash}${query}`
  const retinaSrc = `${baseUrl}${hash}${retinaQuery}`

  return isRetina() ? retinaSrc : src
}
