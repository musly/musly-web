import { useContext } from 'react'
import { LoadingContext } from '../components/LoadingProvider'

export function useLoadingState() {
  const loading = useContext(LoadingContext)
  return loading
}
