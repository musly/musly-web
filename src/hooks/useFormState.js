import { useState, useEffect, useCallback } from 'react'

export function useFormState(initialState, onComplete) {
  const [values, setValues] = useState(initialState)
  // const [errors, setErrors] = useState({})
  const [isSubmitting, setIsSubmitting] = useState(false)
  const [isChanged, setIsChanged] = useState(false)

  // -- isChanged --
  useEffect(() => {
    const isEqual = JSON.stringify(values) === JSON.stringify(initialState)

    if (!isEqual && !isChanged) {
      setIsChanged(true)
    } else if (isEqual && isChanged) {
      setIsChanged(false)
    }
  }, [initialState, isChanged, values])

  // -- isSubmitting --
  useEffect(() => {
    if (!isSubmitting) {
      return
    }

    // if (Object.keys(errors).length === 0) {
    onComplete(values)
    // }
  }, [onComplete, initialState, isSubmitting, values])

  /**
   * Handles changing value of the form input.
   * @param {Object} event The change event object.
   */
  const onChange = useCallback((event) => {
    setValues({
      ...values,
      [event.target.name]: event.target.type === 'checkbox' ? event.target.checked : event.target.value,
    })
  }, [values])

  /**
   * Handles submitting of the form and starts the submit process.
   * @param {Object} event The submit event object.
   */
  const onSubmit = useCallback((event) => {
    event.preventDefault()
    setIsSubmitting(true)
  }, [])

  const onReset = useCallback((event) => {
    event.preventDefault()
    setValues(initialState)
  }, [initialState])

  return {
    onChange,
    onSubmit,
    onReset,
    values,
    isChanged,
  }
}
