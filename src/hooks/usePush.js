import { useCallback } from 'react'
import { history } from 'rooter'

export function usePush(href) {
  const onClick = useCallback((event) => {
    event.preventDefault()
    history.push({ to: href })
  }, [href])

  return onClick
}
