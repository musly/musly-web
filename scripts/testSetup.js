import { i18n } from '../src/utils/i18n'

const enzyme = require('enzyme')
const Adapter = require('enzyme-adapter-react-16')

require('dotenv').config()

enzyme.configure({ adapter: new Adapter() })
i18n.init()
