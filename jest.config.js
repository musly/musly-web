module.exports = {
  clearMocks: true,
  coverageDirectory: 'coverage',
  watchman: true,
  collectCoverageFrom: [
    '**/*.{js,jsx}',
  ],
  snapshotSerializers: [
    'enzyme-to-json/serializer',
  ],
  unmockedModulePathPatterns: [
    'node_modules/react/',
    'node_modules/enzyme/',
  ],
  transformIgnorePatterns: [
    '/node_modules/(?!rooter).+\\.js$',
  ],
  setupFiles: [
    './scripts/testSetup.js',
  ],
  setupFilesAfterEnv: [
    './scripts/frameworkSetup.js',
  ],
}
