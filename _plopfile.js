const PATH_COMMON_COMPONENTS = 'app/musly-common/components'
const PATH_COMMON_HOOKS = 'app/musly-common/hooks'
const PATH_COMMON_ICONS = 'app/musly-common/icons'
const PATH_WEB_COMPONENTS = 'app/musly-web/components'
const PATH_WEB_HOOKS = 'app/musly-web/hooks'
const PATH_WEB_VIEWS = 'app/musly-web/views'

function createComponentActions(path) {
  return [
    {
      type: 'add',
      path: `${path}/{{pascalCase name}}/{{pascalCase name}}.jsx`,
      templateFile: 'templates/Component.js.hbs',
    },
    {
      type: 'add',
      path: `${path}/{{pascalCase name}}/__tests__/{{pascalCase name}}.spec.jsx`,
      templateFile: 'templates/Component-test.js.hbs',
    },
    {
      type: 'add',
      path: `${path}/{{pascalCase name}}/index.js`,
      templateFile: 'templates/Component-index.js.hbs',
    },
    {
      type: 'modify',
      path: `${path}/index.js`,
      pattern: /(\/\/ eof)/gi,
      template: 'export { {{pascalCase name}} } from \'./{{pascalCase name}}\'\n$1',
    },
  ]
}

function createGenericComponentActions() {
  return [
    {
      type: 'add',
      path: 'app/musly-{{dashCase module}}/components/{{pascalCase name}}/{{pascalCase name}}.jsx',
      templateFile: 'templates/Component.js.hbs',
    },
    {
      type: 'add',
      path: 'app/musly-{{dashCase module}}/components/{{pascalCase name}}/__tests__/{{pascalCase name}}.spec.jsx',
      templateFile: 'templates/Component-test.js.hbs',
    },
    {
      type: 'add',
      path: 'app/musly-{{dashCase module}}/components/{{pascalCase name}}/index.js',
      templateFile: 'templates/Component-index.js.hbs',
    },
    {
      type: 'modify',
      path: 'app/musly-{{dashCase module}}/components/index.js',
      pattern: /(\/\/ eof)/gi,
      template: 'export { {{pascalCase name}} } from \'./{{pascalCase name}}\'\n$1',
    },
  ]
}

function createViewActions(path) {
  return [
    {
      type: 'add',
      path: `${path}/{{pascalCase name}}/{{pascalCase name}}.jsx`,
      templateFile: 'templates/ViewComponent.js.hbs',
    },
    {
      type: 'add',
      path: `${path}/{{pascalCase name}}/index.js`,
      templateFile: 'templates/ViewComponent-index.js.hbs',
    },
  ]
}

function createGenericViewActions() {
  return [
    {
      type: 'add',
      path: 'app/musly-{{dashCase module}}/views/{{pascalCase name}}/{{pascalCase name}}.jsx',
      templateFile: 'templates/ViewComponent.js.hbs',
    },
    {
      type: 'add',
      path: 'app/musly-{{dashCase module}}/views/{{pascalCase name}}/index.js',
      templateFile: 'templates/ViewComponent-index.js.hbs',
    },
    {
      type: 'modify',
      path: 'app/musly-{{dashCase module}}/views/index.js',
      pattern: /(\/\/ eof)/gi,
      template: 'export { {{pascalCase name}} } from \'./{{pascalCase name}}\'\n$1',
    },
  ]
}

function createGenericProviderActions() {
  return [
    {
      type: 'add',
      path: 'app/musly-{{dashCase module}}/providers/{{pascalCase name}}Provider/{{pascalCase name}}Provider.jsx',
      templateFile: 'templates/Provider.js.hbs',
    },
    {
      type: 'modify',
      path: 'app/musly-{{dashCase module}}/providers/index.js',
      pattern: /(\/\/ eof)/gi,
      template: 'export { {{pascalCase name}}Provider } from \'./{{pascalCase name}}Provider\'\n$1',
    },
    {
      type: 'add',
      path: 'app/musly-{{dashCase module}}/contexts/{{pascalCase name}}Context.js',
      templateFile: 'templates/Provider-context.js.hbs',
    },
    {
      type: 'modify',
      path: 'app/musly-{{dashCase module}}/contexts/index.js',
      pattern: /(\/\/ eof)/gi,
      template: 'export { {{pascalCase name}}Context } from \'./{{pascalCase name}}Context\'\n$1',
    },
  ]
}

function createHookActions(path) {
  return [
    {
      type: 'add',
      path: `${path}/{{camelCase name}}.js`,
      templateFile: 'templates/Hook.js.hbs',
    },
    {
      type: 'modify',
      path: `${path}/index.js`,
      pattern: /(\/\/ eof)/gi,
      template: 'export { {{camelCase name}} } from \'./{{camelCase name}}\'\n$1',
    },
  ]
}

function createGenericHookActions() {
  return [
    {
      type: 'add',
      path: 'app/musly-{{dashCase module}}/hooks/{{camelCase name}}.js',
      templateFile: 'templates/Hook.js.hbs',
    },
    {
      type: 'modify',
      path: 'app/musly-{{dashCase module}}/hooks/index.js',
      pattern: /(\/\/ eof)/gi,
      template: 'export { {{camelCase name}} } from \'./{{camelCase name}}\'\n$1',
    },
  ]
}

function createIconActions(path) {
  return [
    {
      type: 'add',
      path: `${path}/{{pascalCase name}}Icon.jsx`,
      templateFile: 'templates/Icon.js.hbs',
    },
    {
      type: 'add',
      path: `${path}/__tests__/{{pascalCase name}}Icon.spec.jsx`,
      templateFile: 'templates/Icon-test.js.hbs',
    },
    {
      type: 'modify',
      path: `${path}/index.js`,
      pattern: /(\/\/ eof)/gi,
      template: 'export { {{pascalCase name}}Icon } from \'./{{pascalCase name}}Icon\'\n$1',
    },
  ]
}

module.exports = (plop) => {
  plop.setGenerator('module', {
    description: 'Creates a new module',
    prompts: [
      {
        type: 'input',
        name: 'name',
        message: 'What is your module name (leave out "musly" prefix!)?',
      },
      {
        type: 'input',
        name: 'description',
        message: 'The module description',
      },
    ],
    actions: [
      {
        type: 'add',
        path: 'app/musly-{{dashCase name}}/package.json',
        templateFile: 'templates/Module-package.json.hbs',
      },
      {
        type: 'add',
        path: 'app/musly-{{dashCase name}}/index.js',
        template: '// eof\n',
      },
      {
        type: 'add',
        path: 'app/musly-{{dashCase name}}/components/index.js',
        template: '// eof\n',
      },
      {
        type: 'add',
        path: 'app/musly-{{dashCase name}}/hooks/index.js',
        template: '// eof\n',
      },
      {
        type: 'add',
        path: 'app/musly-{{dashCase name}}/providers/index.js',
        template: '// eof\n',
      },
      {
        type: 'add',
        path: 'app/musly-{{dashCase name}}/views/index.js',
        template: '// eof\n',
      },
    ],
  })
  plop.setGenerator('common-component', {
    description: 'Create a common component',
    prompts: [
      {
        type: 'input',
        name: 'name',
        message: 'What is your component name?',
      },
    ],
    actions: createComponentActions(PATH_COMMON_COMPONENTS),
  })
  plop.setGenerator('web-component', {
    description: 'Create a web component',
    prompts: [
      {
        type: 'input',
        name: 'name',
        message: 'What is your component name?',
      },
    ],
    actions: createComponentActions(PATH_WEB_COMPONENTS),
  })
  plop.setGenerator('common-test', {
    description: 'Create a component test for a common component',
    prompts: [
      {
        type: 'input',
        name: 'name',
        message: 'What\'s the component\'s name?',
      },
    ],
    actions: [
      {
        type: 'add',
        path: `${PATH_COMMON_COMPONENTS}/{{pascalCase name}}/__tests__/{{pascalCase name}}.spec.jsx`,
        templateFile: 'templates/Component-test.js.hbs',
      },
    ],
  })
  plop.setGenerator('web-test', {
    description: 'Create a component test for a web component',
    prompts: [
      {
        type: 'input',
        name: 'name',
        message: 'What\'s the component\'s name?',
      },
    ],
    actions: [
      {
        type: 'add',
        path: `${PATH_WEB_COMPONENTS}/{{pascalCase name}}/__tests__/{{pascalCase name}}.spec.jsx`,
        templateFile: 'templates/Component-test.js.hbs',
      },
    ],
  })
  plop.setGenerator('web-view', {
    description: 'Creates a web view',
    prompts: [
      {
        type: 'input',
        name: 'name',
        message: 'What is the view name?',
      },
    ],
    actions: createViewActions(PATH_WEB_VIEWS),
  })
  plop.setGenerator('common-hook', {
    description: 'Creates a common hook',
    prompts: [
      {
        type: 'input',
        name: 'name',
        message: 'What is the hook\'s name?',
      },
    ],
    actions: createHookActions(PATH_COMMON_HOOKS),
  })
  plop.setGenerator('web-hook', {
    description: 'Creates a web hook',
    prompts: [
      {
        type: 'input',
        name: 'name',
        message: 'What is the hook\'s name?',
      },
    ],
    actions: createHookActions(PATH_WEB_HOOKS),
  })
  plop.setGenerator('icon', {
    description: 'Creates a new icon in the common library',
    prompts: [
      {
        type: 'input',
        name: 'name',
        message: 'What is the icon name?',
      },
    ],
    actions: createIconActions(PATH_COMMON_ICONS),
  })
  plop.setGenerator('component', {
    description: 'Creates a component inside a module',
    prompts: [
      {
        type: 'input',
        name: 'name',
        message: 'What is the component name?',
      },
      {
        type: 'input',
        name: 'module',
        message: 'What is the module name (folder name without the "musly" prefix)?',
      },
    ],
    actions: createGenericComponentActions(),
  })
  plop.setGenerator('view', {
    description: 'Creates a view inside a module',
    prompts: [
      {
        type: 'input',
        name: 'name',
        message: 'What is the component name?',
      },
      {
        type: 'input',
        name: 'module',
        message: 'What is the module name (folder name without the "musly" prefix)?',
      },
    ],
    actions: createGenericViewActions(),
  })
  plop.setGenerator('hook', {
    description: 'Creates a web hook inside a module',
    prompts: [
      {
        type: 'input',
        name: 'name',
        message: 'What is the hook\'s name?',
      },
      {
        type: 'input',
        name: 'module',
        message: 'What is the module name (folder name without the "musly" prefix)?',
      },
    ],
    actions: createGenericHookActions(),
  })
  plop.setGenerator('provider', {
    description: 'Creates a new provider inside a module',
    prompts: [
      {
        type: 'input',
        name: 'name',
        message: 'What is the provider name?',
      },
      {
        type: 'input',
        name: 'module',
        message: 'What is the module name (folder name without the "musly" prefix)?',
      },
    ],
    actions: createGenericProviderActions(),
  })
}
