require('dotenv').config()
const path = require('path')
const webpack = require('webpack')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const HTMLWebpackPlugin = require('html-webpack-plugin')
const TerserPlugin = require('terser-webpack-plugin')
const CompressionWebpackPlugin = require('compression-webpack-plugin')
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer')
const { GenerateSW } = require('workbox-webpack-plugin')

const ENV = process.env.NODE_ENV || 'development'
const DEV = (ENV === 'development')
const ANALYZE = (process.env.ANALYZE === 'true')
const SRC_FOLDER = path.resolve(__dirname, 'src')
const DIST_FOLDER = path.resolve(__dirname, 'dist')

const plugins = [
  new CleanWebpackPlugin(),
  new webpack.optimize.ModuleConcatenationPlugin(),
  new webpack.DefinePlugin({
    'process.env': {
      NODE_ENV: JSON.stringify(ENV),
      LOCALE: JSON.stringify(process.env.LOCALE),
      FIREBASE_API_KEY: JSON.stringify(process.env.FIREBASE_API_KEY),
      FIREBASE_AUTH_DOMAIN: JSON.stringify(process.env.FIREBASE_AUTH_DOMAIN),
      FIREBASE_DATABASE_URL: JSON.stringify(process.env.FIREBASE_DATABASE_URL),
      FIREBASE_PROJECT_ID: JSON.stringify(process.env.FIREBASE_PROJECT_ID),
      FIREBASE_STORAGE_BUCKET: JSON.stringify(process.env.FIREBASE_STORAGE_BUCKET),
      FIREBASE_MESSAGING_SENDER_ID: JSON.stringify(process.env.FIREBASE_MESSAGING_SENDER_ID),
      FIREBASE_APP_ID: JSON.stringify(process.env.FIREBASE_APP_ID),
      FIREBASE_MEASUREMENT_ID: JSON.stringify(process.env.FIREBASE_MEASUREMENT_ID),
      SENTRY_DSN: JSON.stringify(process.env.SENTRY_DSN),
      SENTRY_ENVIRONMENT: JSON.stringify(process.env.SENTRY_ENVIRONMENT),
    },
  }),
  new CopyWebpackPlugin({
    patterns: [
      {
        from: path.resolve(__dirname, 'assets'),
        to: path.resolve(DIST_FOLDER, 'assets'),
      },
    ],
  }),
  new HTMLWebpackPlugin({
    filename: path.resolve(DIST_FOLDER, 'index.html'),
    template: path.resolve(SRC_FOLDER, 'index.html'),
    inject: 'body',
    cache: false,
    minify: !DEV ? {
      collapseWhitespace: true,
      removeComments: true,
      removeRedundantAttributes: true,
      removeScriptTypeAttributes: true,
      removeStyleLinkTypeAttributes: true,
      useShortDoctype: true,
      minifyCSS: true,
    } : false,
  }),
  new webpack.HashedModuleIdsPlugin(),
  new webpack.NamedModulesPlugin(),
  new webpack.HotModuleReplacementPlugin(),
]

if (!DEV) {
  plugins.push(
    new CompressionWebpackPlugin({
      filename: '[path].br[query]',
      algorithm: 'brotliCompress',
      test: /\.js$|\.png$|\.svg$|\.jpg$$/,
      compressionOptions: {
        // zlib’s `level` option matches Brotli’s `BROTLI_PARAM_QUALITY` option.
        level: 11,
      },
      threshold: 10240,
      minRatio: 0.8,
      deleteOriginalAssets: false,
    }),
    new GenerateSW({
      swDest: 'sw.js',
      exclude: [/index.html/],
      clientsClaim: true,
      skipWaiting: true,
    })
  )
}

if (ANALYZE) {
  plugins.push(
    new BundleAnalyzerPlugin()
  )
}

module.exports = {
  mode: ENV,
  entry: {
    app: [
      `./${path.relative(__dirname, path.resolve(SRC_FOLDER, 'index.jsx'))}`,
    ],
  },
  output: {
    filename: DEV ? '[name].js' : '[name].[hash].js',
    chunkFilename: DEV ? '[name].js' : '[name].[chunkhash].js',
    path: DIST_FOLDER,
    publicPath: '/',
  },
  resolve: {
    extensions: ['.json', '.js', '.jsx', '.css', '.scss'],
    alias: {
      'react-dom': '@hot-loader/react-dom',
    },
  },
  plugins,
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'cache-loader',
            options: {
              cacheDirectory: path.resolve(__dirname, '.cache-loader'),
            },
          },
          {
            loader: 'babel-loader',
            options: {
              cacheDirectory: path.resolve(__dirname, '.cache-loader'),
            },
          },
        ],
      },
    ],
  },
  devServer: {
    publicPath: '/',
    historyApiFallback: true,
    contentBase: path.resolve(__dirname, 'dist'),
    progress: true,
    hot: true,
    host: process.env.HOST || '0.0.0.0',
    port: process.env.PORT || 8080,
  },
  devtool: DEV ? 'cheap-module-eval-source-map' : 'source-map',
  performance: {
    hints: DEV ? false : 'warning',
  },
  optimization: {
    usedExports: true,
    sideEffects: true,
    namedModules: true,
    namedChunks: true,
    nodeEnv: process.env.NODE_ENV,
    removeAvailableModules: true,
    splitChunks: {
      chunks: 'all',
      cacheGroups: {
        commons: {
          test: /[\\/]node_modules[\\/]/,
          name: 'common',
          priority: -10,
        },
      },
    },
    minimizer: [
      new TerserPlugin({
        parallel: true,
        extractComments: false,
        terserOptions: {
          ecma: 5,
          keep_fnames: false,
          mangle: true,
          sourceMap: false,
          safari10: false,
          toplevel: false,
          warnings: false,
          output: {
            comments: false,
          },
          parse: {
            shebang: false,
          },
          compress: {
            passes: 3,
            keep_fargs: false,
          },
        },
      }),
    ],
  },
}
