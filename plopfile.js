const addComponent = require('./plop/addComponent')
const addComponentTest = require('./plop/addComponentTest')
const addView = require('./plop/addView')

module.exports = (plop) => {
  plop.setGenerator('component', addComponent)
  plop.setGenerator('component-test', addComponentTest)
  plop.setGenerator('view', addView)
}
